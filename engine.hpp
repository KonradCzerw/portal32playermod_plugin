#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "interface.hpp"
#include "utils.hpp"

class Engine {
private:
	Interface* g_pServerPluginHandler;
	Interface* g_pVEngineServer;

	using _GetClientSteamID = SteamID*(__cdecl*)(void* thisptr, const edict_t* pPlayerEdict, bool bRequireFullyAuthenticated);
	_GetClientSteamID GetClientSteamID = nullptr;

public:
	virtual ~Engine() = default;

	bool Init();
	void Shutdown();
	const char* Name() { return MODULE("engine"); }

public:
	SteamID* GetSteamID(const edict_t* pPlayerEdict, bool bRequireFullyAuthenticated);

public:
	DECL_DETOUR_T(void, LevelInit, const char* pMapName, const char* pMapEntities, const char* pOldLevel, const char* pLandmarkName, bool loadGame, bool background);
	// DECL_DETOUR_T(bool, ClientConnect, edict_t* pEntity, const char* pszName, const char* pszAddress, char* reject, int maxrejectlen);
};

extern Engine* engine;

#endif // ENGINE_HPP
