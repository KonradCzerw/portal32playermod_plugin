#ifndef VARIABLE_HPP
#define VARIABLE_HPP

#include "tier1.hpp"

class Variable {
public:
	Variable();
	Variable(const char* pName);
	~Variable();

	void SetValue(int value);

	void* ThisPtr();

private:
	ConVar *ptr;
};

#endif // VARIABLE_HPP
