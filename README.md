# Portal 2 32 player mod plugin

This plugin is specifically meant as an addition to 32 Player Mod for Portal 2. 

Current features:
- vscript function `GetPlayerName`, returns username of player at index
- vscript function `AddChatCallback`, provided with a string of a callback function x, it will call it simularly to `x(userid, "message text")`  
- vscript function `GetSteamID`, returns the 32 bit integer user-id component of a users' steam id
- vscript function `SetPhysTypeConvar`, sets 'player_held_object_use_view_model' to supplied integer value

If you got any issues, feel free to report in the `issues` tab on gitlab, just don't ping me in the #mod-help channel on DC since that's for the vscript parts lol.

Big thanks to **Nanoman2525** for feedback and the physics convar modification concept!

**Seeking Windows port maintainer!**
