#include "vscript.hpp"

#include "console.hpp"
#include "offsets.hpp"
#include "server.hpp"
#include "engine.hpp"
#include "variable.hpp"

#include <stdio.h>
#include <unordered_set>
#include <map>

std::unordered_set<std::string> chatCallbackNames;
// std::unordered_map<Event, std::unordered_set<std::string>> eventCallbacks;

VScript::VScript() {
	// eventCallbacks.emplace("", std::initializer_list<std::string>());
}

const char* GetPlayerName(int index) {
	return server->GetPlayerName(index);
}

void AddChatCallback(const char* funcName) {
	chatCallbackNames.insert(std::string(funcName));
}

int GetSteamID(int index) {
	edict_t* edict = server->GetPlayerEdict(index);
	if(!edict) return -1;
	return engine->GetSteamID(edict, false)->m_steamid.m_comp.m_unAccountID;
}

void SetPhysTypeConvar(int value) {
	Variable v("player_held_object_use_view_model");
	v.SetValue(value);
}

void SetMaxPortalSeparationConvar(int value) {
	Variable v("portal_max_separation_force");
	v.SetValue(value);
}

// TODO: finish this
// void RegisterEngineEventCallback(int eventType, const char* funcName) {
	// console->Print("STUB: Attempt to add unimplemented callback %d %s\n", eventName, funcName);
// }

REDECL(VScript::CreateVM);
DETOUR_T(IScriptVM*, VScript::CreateVM, ScriptLanguage_t language) {
	IScriptVM* g_pScriptVM = VScript::CreateVM(thisptr, language);
	auto vmInterface = Interface::Create(g_pScriptVM);
	if(vscript->hasToResetVM) {
		vscript->g_pScriptVM = g_pScriptVM;
		vscript->hasToResetVM = false;
	}
	vscript->LookupFunction = vmInterface->Original<_LookupFunction>(Offsets::LookupFunction);
	vscript->ExecuteFunction = vmInterface->Original<_ExecuteFunction>(Offsets::ExecuteFunction);
	ScriptRegisterFunction(g_pScriptVM, GetPlayerName, "Gets player username by index");
	ScriptRegisterFunction(g_pScriptVM, AddChatCallback, "Adds chat callback called with player id and message");
	ScriptRegisterFunction(g_pScriptVM, GetSteamID, "Gets account id component of player steamid by index");
	ScriptRegisterFunction(g_pScriptVM, SetPhysTypeConvar, "Sets 'player_held_object_use_view_model' to supplied integer value");
	ScriptRegisterFunction(g_pScriptVM, SetMaxPortalSeparationConvar, "Sets 'portal_max_separation_force' to supplied integer value");
	// ScriptRegisterFunction(g_pScriptVM, RegisterEngineEventCallback, "Adds an event callback called according to the engine ISERVERPLUGINHELPERS001 interface");
	return g_pScriptVM;
}

bool VScript::Init() {
	this->scriptmanager = Interface::Create(vscript->Name(), "VScriptManager009");
	if(this->scriptmanager) {
		this->scriptmanager->Hook(VScript::CreateVM_Hook, VScript::CreateVM, Offsets::CreateVM);
		return true;
	}
	return false;
}

void VScript::Shutdown() {
	this->scriptmanager->Unhook(Offsets::CreateVM);
	Interface::Delete(this->scriptmanager);
}

void VScript::DoChatCallbacks(int id, char* message) {
	for(auto& callbackName: chatCallbackNames)
		this->Call(callbackName.c_str(), id, message);
}

void VScript::ResetVM() {
	this->hasToResetVM = true;
}

// void VScript::FireEvent(std::string eventName) {
// 	auto event = eventCallbacks.find(eventName);
// 	if(event == eventCallbacks.end()) {
// 		console->Print("No event with name %s found!\n", eventName.c_str());
// 	} else {
// 		char buf[256];
// 		for(auto& n : event->second) {
// 			snprintf(buf, 256, "%s(%d, \"%s\")", callbackName.c_str(), id, message);
			
// 		}
// 	}

// }

VScript* vscript;
